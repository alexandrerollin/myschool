﻿namespace QuestionLib;

public class Question
{
    public string QuestInfo { get; set; }
    public string QuestAnswer { get; set; }
    public int Point { get; set; }
    public Question(string questInfo, string questAnswer, int point)
    {
        QuestInfo = questInfo;
        QuestAnswer = questAnswer;
        Point = point;
    }

    public override bool Equals(object? obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return $"The Question is {QuestInfo} and the answer is {QuestAnswer} for {Point} points";
    }


    public static void SetQuestion(Question question)
    {
        SetInfo();
        SetAnswer();
        SetPoint();
    }

    public static string SetInfo()
    {
        Console.WriteLine("What is the question? : ");
         return Console.ReadLine();
    }
    public static string SetAnswer()
    {
        Console.WriteLine("What is the answer? : ");
        return Console.ReadLine();
    }
    public static int SetPoint()
    {
        Console.WriteLine("What is the amount of point? : ");
        return int.Parse(Console.ReadLine());
    }
    public static Question[] CreateExam()
    {
        Console.WriteLine("How Many Questions? ");
        int counter = int.Parse(Console.ReadLine());
        Question[] exam2 = new Question[counter];

        for (int i = 0; i < exam2.Length; i++)
        {
            Console.WriteLine($"Question #{i + 1}");
            exam2[i] = new Question(SetInfo(), SetAnswer(), SetPoint());
            while (i + 1 < exam2.Length )
            {
                Console.WriteLine("Next Question!\n");
                break;
            }
            Console.WriteLine();
        }
        return exam2;
    }
    
}