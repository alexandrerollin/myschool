﻿namespace TeacherLib;

public class Teacher
{
    public int TeachId { get; set; }
    public string TeachName { get; set; }

    public Teacher(int teachId, string teachName)
    {
        TeachId = teachId;
        TeachName = teachName;
    }
}