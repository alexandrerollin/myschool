﻿using System.Data;
using System.Diagnostics.Metrics;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics.Arm;
using QuestionLib;
using StudentLib;
using TeacherLib;
using static MySchool.Login;
using static QuestionLib.Question;
using static StudentLib.Student;
using static MySchool.PassExam;

namespace MySchool
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] class1 = new Student[3];//basic set;
            class1[0] = new Student(20,"alex",0);
            class1[1]= new Student(15,"bob",0);
            class1[2] = new Student(12,"claire",0);
            
            Student userStudent = VerifyId(class1); // assign userStudent to a student in class1 depending on userID
            
            
            Question[] exam = new Question[3]; //basic set;
            exam[0] = new Question("What is the best number? : ", "42", 5);
            exam[1] = new Question("What is the best city? : ", "Perce", 10);
            exam[2] = new Question("What is the best food? : ", "Pizza", 15);
            
            StudentExam(exam,userStudent); // only one student pass the exam
            Console.WriteLine($"{userStudent.Name} grade = {userStudent.Grade}");
            
            
            Question[] exam2 = CreateExam(); // calls function to ask for user input to create new Question[]
            Student[] class2 = CreateClass(); // calls function to ask for user input to create new Student[]
            
            ClassExam(exam2,class2); // All student of class2 pass the exam exam2
            Console.WriteLine($"{class2[0].Name} grade = {class2[0].Grade}");
            Console.WriteLine(class2[0]); // check if ToString works
        }
    }
}