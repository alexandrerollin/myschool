﻿
namespace StudentLib;

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Grade { get; set; }

    public Student(int id, string name, int grade)
    {
        Id = id;
        Name = name;
        Grade = grade;
    }
    public static void SetStudent(Student student)
    {
        SetID();
        SetName();
        DefaultGrade();
    }

    public override bool Equals(object? obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return $"{Id}: {Name} grade -> {Grade}";
    }

    public static int SetID()
    {
        Console.WriteLine("What is the student ID? : ");
        return int.Parse(Console.ReadLine());
    }
    public static string SetName()
    {
        Console.WriteLine("What is the student name? : ");
        return Console.ReadLine();
    }
    public static int DefaultGrade()
    {
        return 0;
    }
    public static Student[] CreateClass()
    {
        Console.WriteLine("How Many Students? ");
        int counter = int.Parse(Console.ReadLine());
        Student[] class1 = new Student[counter];
            
        for (int i = 0; i < class1.Length; i++)
        {
            class1[i] = new Student(SetID(),SetName(),DefaultGrade());// need to find better way to have a default constructor
            while (i + 1 < class1.Length)
            {
                Console.WriteLine("Next Student!\n");
                break;
            }
            Console.WriteLine();
        }
        return class1;
    }
}