using StudentLib;
using static StudentLib.Student;
namespace MySchool;

public class Login
{
    public static int PrintStart()
    {
        Console.Clear();
        Console.WriteLine("*-------------------*");
        Console.WriteLine("*-------LOGIN-------*");
        Console.WriteLine("*-------------------*");
        Console.WriteLine("*Press [1]-> Student*");
        Console.WriteLine("*Press [2]-> Teacher*");
        Console.WriteLine("*-------------------*");
        int answer = int.Parse(Console.ReadLine());
        if (answer == 1) 
        {
            Console.Clear();
            Console.WriteLine("*------STUDENT------*");
            Console.WriteLine("*-------LOGIN-------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*ENTER STUDENT ID -> ");
            int studentID = int.Parse(Console.ReadLine());
            return studentID;
        }
        if(answer == 2)
        {
            Console.Clear();
            Console.WriteLine("*------TEACHER------*");
            Console.WriteLine("*-------LOGIN-------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*-------------------*");
            Console.WriteLine("*ENTER TEACHER ID -> ");
            int teacherID = int.Parse(Console.ReadLine());
            return teacherID;
        }
        return 0;
    }
    public static Student VerifyId(Student[] otherClass)
    {
        Student userStudent = new Student(0,"",0);
        do
        {
            int tempID = PrintStart();
            for (int i = 0; i < otherClass.Length; i++)
            {
                if (tempID == otherClass[i].Id)
                {
                    userStudent = otherClass[i];
                }
                    
            }
            if(userStudent.Id == 0)
            {
                Console.Clear();
                Console.WriteLine("*------STUDENT------*");
                Console.WriteLine("*-------LOGIN-------*");
                Console.WriteLine("*-------------------*");
                Console.WriteLine("*----ID NOT FOUND---*");
                Console.WriteLine("*--CREATE STUDENT?--*");
                Console.WriteLine("*-------(Y/N)-------*");
                char answer = char.Parse(Console.ReadLine().ToUpper());
                if (answer == 'Y')
                {
                    Console.WriteLine("PLEASE ENTER STUDENT INFO : ");
                    userStudent = new Student(SetID(),SetName(),DefaultGrade());
                }

            }
        } while (userStudent.Id == 0);

        return userStudent;
    }
    
    
}