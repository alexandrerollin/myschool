using QuestionLib;
using StudentLib;

namespace MySchool;

public class PassExam
{
    public static void ClassExam(Question[] myExam, Student[] myClass) // all student of Student[] myClass
    {
        for (int i = 0; i < myClass.Length; i++) // all students one by one
        {
            Console.WriteLine($"It is {myClass[i].Name} turn to answer the exam\n");
            for (int j = 0; j < myExam.Length; j++) // answer all question of exam
            {
                Console.WriteLine(myExam[j].QuestInfo);
                Console.Write("Answer : ");
                string answer = Console.ReadLine();
                if (answer.ToUpper() == myExam[i].QuestAnswer.ToUpper()) // this should be the isEquals fonction instead
                {
                    myClass[i].Grade += myExam[i].Point; // grade of each student
                }
            }
        }
    }
    public static void StudentExam(Question[] myExam, Student student) // Only one student
    {
           
        Console.WriteLine($"It is {student.Name} turn to answer the exam\n");
        for (int j = 0; j < myExam.Length; j++) // answer all question of exam
        {
            Console.WriteLine(myExam[j].QuestInfo);
            Console.Write("Answer : ");
            string answer = Console.ReadLine();
            if (answer.ToUpper() == myExam[j].QuestAnswer.ToUpper()) // this should be the isEquals fonction instead
            {
                student.Grade += myExam[j].Point; // grade of student
            }
        }
    }
}